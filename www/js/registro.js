function validateEmail(email) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(email);
}

function finalizar_registro() {
    if( $$('#form-registro [name="nombre"]').val() == '' ){
        myApp.alert( 'Captura un nombre.', ' ' );
        return false;
    }
    
    if( $$('#form-registro [name="apellidos"]').val() == '' ){
        myApp.alert( 'Captura tus apellidos.', ' ' );
        return false;
    }
    
    if( $$('#form-registro [name="correo"]').val() == '' ){
        myApp.alert( 'Captura tu correo.', ' ' );
        return false;
    }else{
        var correo = $$('#form-registro [name="correo"]').val();
        if( !validateEmail( correo ) ){
            myApp.alert('Captura un correo v&#225;lido.', ' ');
            return false;
        }
    }
    
    if( $$('#form-registro [name="usuario"]').val() == '' ){
        myApp.alert( 'Captura tu usuario.', ' ' );
        return false;
    }
    
    if( $$('#form-registro [name="password"]').val() == '' ){
        myApp.alert( 'Captura tu password.', ' ' );
        return false;
    }
    
    if( $$('#form-registro [name="password_eco"]').val() == '' ){
        myApp.alert( 'Repite tu password.', ' ' );
        return false;
    }
    
    if ( $$('#form-registro [name="password"]').val() != $$('#form-registro [name="password_eco"]').val() ) {
        myApp.alert('Las contraseñas no coinciden.', ' ');
        return false;
    }
    
    var url = ruta_app + 'usuarios/registro';
    
    var formData = myApp.formToJSON('#form-registro');
    
    // alert(formData);
    
    $$.ajax({
        url: url,
        type:'POST',
        dataType:'json',
        data:formData,
        success: function (response) {
            myApp.alert(response.mensaje,' ');
            
            if( response.codigo == 0 ){    
                setTimeout(function  (arguments) {
                    cargar_pagina('login');
                }, 1500);
            }
        },
        error: function  () {
            myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.',' ');
        }
    });
    
    
}

function agregarFoto(imageURI) {
    $$('#img_registro').attr('src', "data:image/jpeg;base64," + imageURI);
    $$('#hidden_imagen_registro').val(imageURI);
}

function nueva_imagen () {
    navigator.camera.getPicture(agregarFoto, onFail, {
        quality: 30,
        targetWidth: 600,
        targetHeight: 600,
        destinationType: Camera.DestinationType.DATA_URL,
        saveToPhotoAlbum: true
    });
}