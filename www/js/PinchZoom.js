myApp.onPageInit('PinchZoom', function (page) {
    //  alert(" pin ReInit");
    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link back"><span class="font-gris-fuerte icon-top-menu-size icon-custom-icons-10"></span></a></div><div class="center sliding"></div><div class="right"></div></div>');

    myApp.showPreloader('Cargando...');

  
    var contents = '<div class="pinch-zoom"><img src="'+ window.localStorage.getItem("imgurlZoom")+'" /></div>';
    $$('[data-page="PinchZoom"] .page-content').html(contents);

    $('div.pinch-zoom').each(function () {
        new RTP.PinchZoom($(this), {});
    });

    setTimeout(function () { myApp.hidePreloader(); }, 2000);


});