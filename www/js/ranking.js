$("#sidebar").insertAfter(".navbar");
var ArtishName = '';
var AllSelectedArtist = [];
var Userposicion = [];
var Artranking_point = '';
var artist_get_id = 0;
myApp.onPageInit('ranking', function (page) {
    myApp.showPreloader('Cargando...');
        var url = ruta_app + 'ranking/get/10';
        var id_usuario = get_item('id_usuario');
        var ah = '';
        $$.ajax({
            url: url,
            type: 'POST',
            dataType: 'json',
            data: { id_usuario: id_usuario },
            success: function (response) {
                $$('[data-page="ranking"] .page-content').html('');
                myApp.hidePreloader();
                if (response.codigo == 0) {

                    var artistas = response.artistas;
                    AllSelectedArtist = artistas;
                    console.log(AllSelectedArtist);
                    var artista_points = (response.artista_points == null ? 0 : response.artista_points);
                    var c = '<div class="inner-content-padding header-ranking"><br/><br/><div class="swiper-container swiper-init" data-speed="400" data-space-between="10" data-pagination=".swiper-pagination" data-slides-per-view="3"><div class="swiper-wrapper custom-wrapper">';
                    $.each(artistas, function (index, swipe) {

                        if (index < artistas.length) {
                            var artista = artistas[index];

                            ArtishName = artistas[0].nombre_artista;
                            ranking_point = '0';
                            $.each(artista_points, function (index2, swipe2) {
                                if (index2 < artista_points.length) {
                                    var artista_point9 = artista_points[index2];
                                    Artranking_point = artista_points[index2].points;
                                    if (artista_point9.artista_id == artista.id_artista) {

                                        ranking_point = artista_point9.points;
                                        //break;
                                    }
                                }
                            });
                            var string = artista.imagen.search("promozite.com/checkfans/");
                            if (string == -1) {
                                var imagesname = ruta_app + 'res/images/artistas/' + artista.imagen;
                            } else {
                                var imagesname = artista.imagen;
                            }
                            if (index == 0) {
                                var testk = 'style="border:solid 4px #fff"';
                            }
                            c += '<div class="swiper-slide custom-slide"><div class="img-user"><figure class="artist_all artist_one_css_'+artista.id_artista+'" '+testk+'><img src="' + imagesname + '" onclick="setRank(' + ranking_point + ',' + index + ',' + artista.id_artista + ');" alt=""></figure></div><p class="text-center font-black text-claro">' + artista.nombre_artista + '<br><span class="ranking-point' + index + '"></span></p></div>';
                            ranking_point = '0';

                            // artist ranking users list start
                            artist_get_id = artista.id_artista;
                            var usuarios1 = response.usuarios;
                            var display = 'none';

                            if (index == 0) {
                                display = 'block';
                            }
                            ah += '<div class="artist_' + artist_get_id + ' artistid" style="display:' + display + '">';

                            var usuarios = usuarios1[artist_get_id];

                            if (usuarios.length > 0) {
                                $.each(usuarios, function (index1, swipe1) {

                                    if (index1 < usuarios.length) {
                                        var usuarios9 = usuarios[index1];
                                        //myApp.alert('Size of usuarios ==> '+usuarios.length,'Size');
                                        //console.log(JSON.stringify(usuarios9));
                                        var imgd = '';
                                        if (usuarios9.imagen != null) {
                                            imgd = ' <img src="http://promozite.com/checkfans/contenido/user_img?id=' + usuarios9.id_usuario + '"  alt="">';
                                        }
                                        else {                                            
                                            imgd = ' <img src="images/user-bg.svg" alt="">';                                     
                                        }
                                      
                                        var c1 = index1 + 1;
                                        if (index1 == 0) {
                                            ah += '<div class="ranking-row-destacado"><div class="posicion">' + c1 + '</div><div class="img-user"><figure> '+imgd +'</figure></div><div class="name-user">' + usuarios9.usuario + '</div><div class="acumulados"><img src="images/copa.svg"></div></div>';
                                        }
                                        else {
                                            if (index1 == 1) {
                                                ah += '<div class="text-center">';
                                            }
                                            var id_usuario1 = get_item('id_usuario');
                                            if (usuarios9.id_usuario == id_usuario1) {
                                                Userposicion[artist_get_id] = c1;
                                                class12 = 'toolbar no-border ranking';
                                                class13 = '<div id="sidebar" class="ranking-row-destacado">';
                                                class14 = 'style="position:fixed;bottom: 0;left: 0;"';
                                                class15 = '</div>';
                                            }
                                            else {
                                                class12 = 'ranking-row-general';
                                                class13 = '';
                                                class14 = '';
                                                class15 = '';
                                            }
                                            ah += '<div class="' + class12 + '" ' + class14 + '>' + class13 + '<div class="posicion">' + c1 + '</div><div class="img-user"><figure>' + imgd + '</figure></div><div class="name-user">' + usuarios9.usuario + '</div><div class="acumulados"><i class="font-naranja icon-custom-icons-02"></i><small>' + usuarios9.puntos + '</small></div></div>' + class15;
                                            if (index1 == usuarios.length - 1) {
                                                if (usuarios.length > 2) {
                                                    ah += '</div>';
                                                }
                                            }
                                        }
                                    }

                                    //    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-blanco"><span class="font-blanco icon-top-menu-size icon-custom-icons-23"></span></a></div><div class="center sliding font-blanco">RANKING</div><div class="right"  ><a class="link icon-only force-padding"><div id="RankShareIcn" onclick="app_f.sharewithFB(\'Estoy' + $$('#sidebar .posicion').html() + '\',\'en el ranking de los m&#225;s fans de ' + ArtishName + '\')"  class="font-blanco icon-top-menu-size icon-custom-icons-06"></div></a></div></div>');


                                });
                            } else {

                                ah += '<div class="ranking-row-general" style="color: #ff750a;"><h3>No hay fans interactuando para este artista</h3> </div>';
                            }

                            ah += '</div>';
                            // artist ranking users list end

                        }
                    });
                    c += '</div></div></div>';
                    c += ah;

                    $$('[data-page="ranking"] .page-content').html(c);
                    swiper = new Swiper('.swiper-container', {
                        direction: 'horizontal',
                        slidesPerView: 3,
                    });

                }
                $$('body').on('click', '#search1', function () {
                    $(".search").html('<input type="text"/>');
                });

                var posicion = Userposicion[AllSelectedArtist[0].id_artista] != null ? Userposicion[AllSelectedArtist[0].id_artista] : 0;

                var clevn = '';
                if (posicion != "0")
                    clevn = ' app_f.sharewithFB(\'Estoy ' + posicion + '\',\'en el ranking de los m&#225;s fans de ' + ArtishName + '\')';
                else
                    clevn = 'elseMessage()';

                $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-blanco"><span class="font-blanco icon-top-menu-size icon-custom-icons-23"></span></a></div><div class="center sliding font-blanco">Ranking Mensual</div><div class="right"  ><a class="link icon-only force-padding"><div id="RankShareIcn" onclick="' + clevn + '"  class="font-blanco icon-top-menu-size icon-custom-icons-06"></div></a></div></div>');


            },
            error: function () {
                myApp.hidePreloader();
                myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.', ' ');
         
            }
        });






});

function elseMessage() {
    myApp.alert('No posee puntos para este artista todavia. Empieza a sumar !', 'Informaci&#243;n');
}

function setRank(ranking_point, index3, artist_id) {
    myApp.showPreloader('Cargando...');
    setTimeout(function () {

        $('.artistid').hide();
        $('.artist_all').css('border','none');
        $('.artist_one_css_'+artist_id).css('border','solid 4px #fff');
        $('.artist_' + artist_id).show();
        // $(".ranking-point"+index3+"").html(ranking_point);
        ArtishName = AllSelectedArtist[index3].nombre_artista;
        var posicion = Userposicion[artist_id] != null ? Userposicion[artist_id] : 0;
        if (parseInt(posicion) > 0)
            $('#RankShareIcn').attr('onclick', 'app_f.sharewithFB(\'Estoy ' + posicion + '\',\'en el ranking de los m&#225;s fans de ' + ArtishName + '\')');
        else
            $('#RankShareIcn').attr('onclick', 'elseMessage()');



        myApp.hidePreloader();
    }, 100);
}