function verificar_login(){
    var login_ok = window.localStorage.getItem("login_ok");
    
    if(login_ok){
        // var mainView = myApp.addView('.view-main');
        // mainView.router.loadPage('seleccion_artista.html');
        pintarImagenPerfil();
        if (window.localStorage.getItem("artist_id786") != null) {
            cargar_pagina('Feed');
        }
        else { cargar_pagina('seleccion_artista'); }
        $$('h4#globalUsername').html(window.localStorage.getItem("usuarioName"));
        globalwelcome.close();
    } 
}

function logout() {    
    window.localStorage.removeItem("login_ok");
    window.localStorage.removeItem("id_usuario");
    window.localStorage.removeItem("usuario");
    window.localStorage.removeItem("imagen");
    window.localStorage.removeItem("contenido_favourite");
    window.localStorage.removeItem("notification_status");
    window.localStorage.removeItem("login_FB");
    window.localStorage.removeItem("usuario_nm");
    window.localStorage.removeItem("artist_id786");
    window.localStorage.removeItem("FeedActiveIndex");
    window.localStorage.removeItem("FeedIndexID");
    var mainView = myApp.addView('.view-main');
    globalwelcome.open();
    app_f.logout();
    mainView.router.loadPage('index.html');
}

function get_item(key) {
    return window.localStorage.getItem(key);
}

function set_item(key, value) {
    window.localStorage.setItem(key, value);
}

function login_mail () {
    var usuario = $$('#form_login input[name="usuario"]').val();    
    if(usuario == ''){
        myApp.alert('Por favor captura un usuario', ' ');
        return 0;
    }    
    var password = $$('#form_login input[name="password"]').val();    
    if(password == ''){
        myApp.alert('Por favor captura un password', ' ');
        return 0;
    }    
    var url = ruta_app + 'session/login_usuario_mail';
    myApp.showPreloader('Cargando...');    
    $$.post(url, { usuario: usuario, password: password, gcm_id: window.localStorage.getItem('gcmId') }, function (data) {
        
        data = JSON.parse(data);
        
        if(data.codigo == 0){
            var usuario = data.usuario;
         //   alert(data.codigoartista);
            window.localStorage.setItem("login_ok", true);
            window.localStorage.setItem("id_usuario", usuario.id_usuario);
            window.localStorage.setItem("usuario", usuario.usuario);
          //  window.localStorage.setItem("imagen", usuario.imagen);
            window.localStorage.setItem("usuarioName", usuario.nombre + " " + usuario.apellidos);
            window.localStorage.setItem("artist_id786",  data.codigoartista);
            window.localStorage.setItem("contenido_favourite", usuario.post_id);
            window.localStorage.setItem("notification_status", usuario.notification_status);
            $$('h4#globalUsername').html(usuario.nombre + " " + usuario.apellidos);
            pintarImagenPerfil();
            
            getPuntosRecord();
            var mainView = myApp.addView('.view-main');
            if (usuario.first_login == 1) {
                mainView.router.loadPage('seleccion_artista.html');
            }
            else {
                mainView.router.loadPage('Feed.html');
            }
        }else{
            myApp.alert('Usuario y/o contraseña incorrectos', ' ');
        }
        myApp.hidePreloader();
        
    }, function () {
        //myApp.alert(response+' '+' '+status+' '+headers+' '+config, ' ');
        myApp.alert('Error de comunicaci &#243;n, verifica tu conexi &#243;n.', ' ');
        myApp.hidePreloader();
    });
    
}

//function add_favourite(contenido) {
//    var usuario = window.localStorage.getItem('id_usuario');
//    if ($$('.self-' + contenido).hasClass('heart-blank')) {
//        $$('.self-' + contenido).removeClass('heart-blank');
//        $$('.self-' + contenido).addClass('heart-fill');
//    }
//    else if ($$('.self-' + contenido).hasClass('heart-fill')) {
//            $$('.self-' + contenido).removeClass('heart-fill');
//            $$('.self-' + contenido).addClass('heart-blank');
//        }
//    if ($$('.self-' + contenido).hasClass('heart-red')) {
//        $$('.self-' + contenido).removeClass('heart-red');
//        $$('.self-' + contenido).addClass('heart-white');
//    } else {
//        if ($$('.self-' + contenido).hasClass('heart-white')) {
//            $$('.self-' + contenido).removeClass('heart-white');
//            $$('.self-' + contenido).addClass('heart-red');
//        }
//    }
//    var url = ruta_app + 'usuarios/set_favourite';
//    $$.post(url, { id_usuario: usuario, id_contenido: contenido }, function (data) {
//        data = JSON.parse(data);
//        if (data.codigo == 0) {
//            var usuario = data.usuario;
//            window.localStorage.setItem("login_ok", true);
//        } else {
//            myApp.alert('Usuario y/o contraseña incorrectos', ' ');
//        }
//    });
//}
myApp.onPageInit('forgotpassword', function (page) {
  
    $$('#btn-forget-mail').click(function () {
        forget_mail();
    });
    $('.navbar').html(' <div class="navbar-inner"><div class="left"><a href="#" class="link back"><div class="font-gris-fuerte icon-top-menu-size icon-custom-icons-10"></div></a></div><div class="center sliding"></div><div class="right"></div></div>');
});
function forget_mail() {
    var em = $$('#form_forget [name="correo"]').val();
    if ($$('#form_forget [name="correo"]').val() == '') {
        myApp.alert('Captura tu correo.', ' ');
        return false;
    } else {
        var correo = $$('#form_forget [name="correo"]').val();
        if (!validateEmail(correo)) {
            myApp.alert('Captura un correo v&#225;lido.');
            return false;
        }
    }
    myApp.showPreloader('Cargando...');
    var url = ruta_app + 'usuarios/ForgetPassword';
    $$.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: { correo: em },
        success: function (response) {
            myApp.hidePreloader();
            myApp.alert('La contraseña se ha enviado en la direcci&#243;n de correo electr&#243;nico.', 'Information');
            if (response.codigo == 1) {
                setTimeout(function (arguments) {
                    cargar_pagina('login');
                }, 1500);
            }
            else {
                myApp.alert('La direcci&#243;n de correo electr&#243;nico no existe.', 'Information');
            }
        },
        error: function () {
            myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.', ' ');
        }
    });
}