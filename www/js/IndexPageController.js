/*jslint browser: true*/
/*global console*/

var app = myapp || {};

var globalwelcome = null;


myapp.pages.IndexPageController = function (myapp, $$) {
    'use strict';
    var botones = '<div class="text-center menu-inicio"><img class="img-responsive" src="images/logo-big.svg" alt="" /><p>Conectar con</p>';
    botones += '<a href="#" class="btn-base btn-blanco btn-login-facebook"><div class="copy"><span class="icon-custom-icons-18 icon-margin" onclick="javascript:app_f.login();"></span>Facebook</div></a>';
    //  botones += '<a href="#" class="btn-base btn-blanco"><div class="copy"><span class="icon-custom-icons-13 icon-margin"></span>Twitter</div></a>';
    //botones += '<a href="#" class="btn-base btn-blanco"><div class="copy"><span class="icon-custom-icons-14 icon-margin"></span>Instagram</div></a>';
    botones += '<a href="#" class="btn-base btn-login-mail btn-blanco"><div class="copy"><span class="icon-custom-icons-17 icon-margin"></span>e-mail</div></a>';
    botones += '<br /><small>Términos y Condiciones  |  Aviso de Privacidad</small></div>';

    // Init method
    (function () {
        var options = {
            'bgcolor': '#f16950',
            'fontcolor': '#fff',
            'onOpened': function () {
                console.log("welcome screen opened");
                $$('.btn-login-mail').click(function () {
                    mostrar_pre_login();
                });
                $$('.btn-login-facebook').click(function () {
                    callFBLogins();
                });
            },
            'onClosed': function () {
                console.log("welcome screen closed");
            }
        },
        welcomescreen_slides = [
          {
              id: 'slide0',
              picture: '<div class="bloque-slider bloque-slider-1"></div>',
              text: 'Seguí a tus artistas favoritos y mira contenido exclusivo.'
          },

          {
              id: 'slide1',
              picture: '<div class="bloque-slider bloque-slider-2"></div>',
              text: 'Convertite en el fan Nº1 del ranking CheckFans'
          },

          {
              id: 'slide2',
              picture: '<div class="bloque-slider bloque-slider-3"></div>',
              text: botones
          }
        ],
        welcomescreen = myapp.welcomescreen(welcomescreen_slides, options);

        globalwelcome = welcomescreen;

        function mostrar_pre_login() {
            welcomescreen.close();
            var mainView = myApp.addView('.view-main');
            mainView.router.loadPage('pre_inicio.html');
        }


        function mostrar_login_mail() {
            welcomescreen.close();
            var mainView = myApp.addView('.view-main');
            mainView.router.loadPage('login.html');
        }
        function callFBLogins() {
            myApp.showPreloader('Cargando...');
            try {
                facebookConnectPlugin.login(["public_profile", "email"],
                   function (response) {
                       window.localStorage.setItem("login_ok", true);
                       window.localStorage.setItem("login_FB", true);
                       window.localStorage.setItem("id_usuario_fB", response.authResponse.userID);
                       window.localStorage.setItem("FB_AccessToken", response.authResponse.accessToken);
                       setTimeout(function () {
                           facebookConnectPlugin.api("me/?fields=id,email,last_name,first_name,picture", ["public_profile"],
             function (result) {
                 var dt = result;
                 window.localStorage.setItem("usuario_nm", dt.first_name + " " + dt.last_name);
                 $$('h4#globalUsername').html(dt.first_name + " " + dt.last_name);
                 window.localStorage.setItem("usuarioName", dt.first_name + " " + dt.last_name);
             //    alert(  window.localStorage.getItem('gcmId'));
                 var surl = "http://promozite.com/checkfans/usuarios/fb_register";
                 var email = dt.email;// "pawanjatav@rediffmail.com";
                 var firstname = dt.first_name; //;"Pawan"
                 var lastname = dt.last_name;//  "Jatav";
                 $$.ajax({
                     url: surl,
                     type: 'POST',
                     dataType: 'json',
                     data: { email: email, firstname: firstname, lastname: lastname, gcm_id: window.localStorage.getItem('gcmId') },
                     success: function (response) {
                         console.log(JSON.stringify(response));
                         if (response.codigo == 0) {

                             var usuario = response.usuario;
                             var codigoartista = response.artistas;
                             window.localStorage.setItem("login_ok", true);
                             window.localStorage.setItem("id_usuario", usuario.id_usuario);
                             window.localStorage.setItem("usuario", usuario.usuario);
                             if (codigoartista != null) {
                                 window.localStorage.setItem("artist_id786", codigoartista);
                             }
                             window.localStorage.setItem("contenido_favourite", usuario.post_id);
                             window.localStorage.setItem("notification_status", usuario.notification_status);
                             pintarImagenPerfil();
                             welcomescreen.close();
                             getPuntosRecord();
                            // alert(usuario.first_login + " " + codigoartista)
                             var mainView = myApp.addView('.view-main');
                             if ((usuario.first_login === "1" || usuario.first_login === "0") && codigoartista == null) {
                                 mainView.router.loadPage('seleccion_artista.html');
                             } else {
                                 mainView.router.loadPage('Feed.html');
                             }
                         }
                         myApp.hidePreloader();
                     },

                     error: function (response, status, headers, config) {
                         console.log(response);
                         myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.', ' '); myApp.hidePreloader();
                     }
                 });

             },
             function (error) {
                 // alert("Failed: " + error);
             });
                       }, 500);

                   },
                   function (response) { myApp.alert(JSON.stringify(response), ' '); });




            } catch (e) {
                console.log(JSON.stringify(e));
            }
        }

        $$(document).on('click', '.tutorial-close-btn', function () {
            welcomescreen.close();
        });

        $$('.tutorial-open-btn').click(function () {
            welcomescreen.open();
        });

        $$(document).on('click', '.tutorial-next-link', function (e) {
            welcomescreen.next();
        });

        $$(document).on('click', '.tutorial-previous-slide', function (e) {
            welcomescreen.previous();
        });

    }());

};

//  
