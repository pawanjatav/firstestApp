/*jslint browser: true*/
/*global console, Framework7, angular, Dom7*/

var myapp = myapp || {};

myapp.pages = myapp.pages || {};

myapp.init = (function () {
    'use strict';
    
    var exports = {};
    
    document.addEventListener("DOMContentLoaded", function(event) { 
        // Initialize app
        var fw7App = new Framework7(),
        fw7ViewOptions = {
            dynamicNavbar: true,
            domCache: true
        },
        mainView = fw7App.addView('.view-main', fw7ViewOptions),
        $$ = Dom7,
        ipc = new myapp.pages.IndexPageController(fw7App, $$);
        
        //   Agregar todos los eventos
        $$('#btn-login-mail').click(function () {
            login_mail();
        });
        
        $$('#btn-logout').click(function  () {
            logout();
        });
        verificar_login();
        
    });
    
    return exports;
    
    
}());




