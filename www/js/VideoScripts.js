﻿var $video_player, _player, _isPlaying = false;
jQuery(document).ready(function ($) {
    jQuery(".fancy_video").fancybox({
        // set type of content (we are building the HTML5 <video> tag as content)
        type: "html",
        // other API options
        scrolling: "no",
        fitToView: false,
        autoSize: false,
        maxWidth: 300,
        opacity: true,
        overlayShow: true,
        beforeLoad: function () {
            // build the HTML5 video structure for fancyBox content with specific parameters
            //    console.log(JSON.stringify($(this.element).data("Instavedio")));
            myApp.showPreloader('Cargando...');
            this.content = "<video id='video_player'  poster='" + $(this.element).data("imgs") + "'  src='" + $(this.element).data("poster") + "' width='100%' Height='90%'  controls='controls' preload='none' ></video>";
            // set fancyBox dimensions
            this.width = 360; // same as video width attribute
            this.height = 360; // same as video height attribute
        }, afterLoad: function () { myApp.hidePreloader(); },
        afterShow: function () {
            // initialize MEJS player
            var $video_player = new MediaElementPlayer('#video_player', {
                defaultVideoWidth: this.width,
                defaultVideoHeight: this.height,
                success: function (mediaElement, domObject) {
                    _player = mediaElement; // override the "mediaElement" instance to be used outside the success setting
                    _player.load(); // fixes webkit firing any method before player is ready
                    _player.play(); // autoplay video (optional)
                    _player.addEventListener('playing', function () {
                        _isPlaying = true;
                    }, false);
                } // success
            });

        },
        beforeClose: function () {
            // if video is playing and we close fancyBox
            // safely remove Flash objects in IE
            if (_isPlaying && navigator.userAgent.match(/msie [6-8]/i)) {
                // video is playing AND we are using IE
                _player.remove(); // remove player instance for IE
                _isPlaying = false; // reinitialize flag
            };
        }
    });
});


// FaceBook
jQuery(document).ready(function ($) {
    jQuery(".fancy_FBvideo").fancybox({
        // set type of content (we are building the HTML5 <video> tag as content)
        type: "html",
        // other API options
        scrolling: "no",
        fitToView: false,
        autoSize: false,
        maxWidth:280,
        opacity: true,
        overlayShow: true,
        beforeLoad: function () {

            myApp.showPreloader('Cargando...');
            this.content = "<div id='video_FBplayer' style='width:100%;'  >" + $(this.element).data("poster") + "</div>";
            this.content = ' <iframe src="' + $(this.element).data("poster") + '" width="100%" height="100%" style="border:none;overflow:hidden" scrolling="no" frameborder="0" allowTransparency="true"></iframe>';
            // set fancyBox dimensions
            this.width = 250; // same as video width attribute
            this.height = 360; // same as video height attribute
        },
        afterLoad: function () { myApp.hidePreloader(); },
    });

    jQuery(".fancy_PinchZoom").fancybox({
        // set type of content (we are building the HTML5 <video> tag as content)
        type: "html",
        // other API options
        scrolling: "no",
        fitToView: false,
        autoSize: false,
        maxWidth:320,
        opacity: true,
        overlayShow: true,
        beforeLoad: function () {
            myApp.showPreloader('Cargando...');
               this.content = '<div class="pinch-zoom"><img src="' + $(this.element).data("imgs") + '" /></div>';
            // set fancyBox dimensions
            this.width = 500; // same as video width attribute
            this.height = 500; // same as video height attribute
        },

        afterShow: function () {        
           $('div.pinch-zoom').each(function () {
                new RTP.PinchZoom($(this), {});
            });
        },
        afterLoad: function () { myApp.hidePreloader(); },
    });


    $(".various").fancybox({
        'padding': 0,
        'autoScale': false,
        'transitionIn': 'none',
        'transitionOut': 'none',
        'title': this.title,
        'width': 640,
        'height': 385,
     //   'href': this.href.replace(new RegExp("watch\\?v=", "i"), 'v/'),
        'type': 'swf',
        'swf': {
            'wmode': 'transparent',
            'allowfullscreen': 'true'
        }
    });
});