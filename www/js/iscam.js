// Initialize your app
var myApp = new Framework7();

// Export selectors engine
var $$ = Dom7;


function onBackKeyDown(e){ 
    e.preventDefault();
} 

document.addEventListener("backbutton", onBackKeyDown, false);

// Add view
var mainView = myApp.addView('.view-main', {
    // Because we use fixed-through navbar we can enable dynamic navbar
    dynamicNavbar: true
});

// Callbacks to run specific code for specific pages, for example for About page:
myApp.onPageInit('about', function (page) {
    // run createContentPage func after link was clicked
    $$('.create-page').on('click', function () {
        createContentPage();
    });
});

// Generate dynamic page
var dynamicPageIndex = 0;
function createContentPage() {
	mainView.router.loadContent(
        '<!-- Top Navbar-->' +
        '<div class="navbar">' +
        '  <div class="navbar-inner">' +
        '    <div class="left"><a href="#" class="back link"><i class="icon icon-back"></i><span>Back</span></a></div>' +
        '    <div class="center sliding">Dynamic Page ' + (++dynamicPageIndex) + '</div>' +
        '  </div>' +
        '</div>' +
        '<div class="pages">' +
        '  <!-- Page, data-page contains page name-->' +
        '  <div data-page="dynamic-pages" class="page">' +
        '    <!-- Scrollable page content-->' +
        '    <div class="page-content">' +
        '      <div class="content-block">' +
        '        <div class="content-block-inner">' +
        '          <p>Here is a dynamic page created on ' + new Date() + ' !</p>' +
        '          <p>Go <a href="#" class="back">back</a> or go to <a href="services.html">Services</a>.</p>' +
        '        </div>' +
        '      </div>' +
        '    </div>' +
        '  </div>' +
        '</div>'
    );
	return;
}

// var myApp = new Framework7();
//  
// var $$ = Dom7;
 
$$('.open-info').on('click', function () {
  myApp.pickerModal('.picker-info')
});
$$('.close-info').on('click', function () {
  myApp.closeModal('.picker-info')
});    

var macro_id = 0;
var cat = -1;
var cat_id = -1;
//var root_url = 'http://localhost/iscam/wp-json/iscam_plugin/v1/';
//var root_url = 'http://www.promozite.com/iscam/wp-json/iscam_plugin/v1/';

var root_url = 'http://consejeroscomerciales.com/wp-json/iscam_plugin/v1/';

var arreglo_macros = Array();
var arreglo_canastos = Array();

myApp.onPageInit('macros', function (e) {
    // run createContentPage func after link was clicked
    $$('#lista_menus').html('');
    arreglo_macros = [];
    var url = root_url + 'macros';
    var titulo; 
    $$.getJSON(url, function (data) {
        data = JSON.parse(data);
        var contenido = '';
        $$.each(data.macros, function(i,macro){
            if(macro.activo != 'no'){
                if(macro.post_title.indexOf(',') > -1){
                    titulo = macro.post_title.split(',');
                    macro.titulo = titulo[0];
                    macro.periodo = titulo[1];
                }
                console.log(macro);
                
                arreglo_macros[ macro.ID ] = macro;
                contenido += ''+
                '<a href="detalle-macro.html" '+
                'onclick="set_macro_id('+macro.ID+')" '+
                'class="boton_macro button custom-buttom-big">'+
                macro.titulo + '</a>'+
                '';
            }
        });
        
        $$('#lista_menus').html(contenido);
    });
});

myApp.onPageInit('detalle-macro', function (e) {
    var macro = arreglo_macros[macro_id];
    $$('#titulo_detalle_macro').html(macro.titulo);
    $$('#contenido_periodo_macro').html(macro.periodo);
    $$('#contenido_detalle_macro').html(macro.post_content);
    
});

myApp.onPageInit('indicadores', function (e) {
    
    $$('#tabla_indicadores .body-table').remove();
    
    var url = root_url + 'indicadores';
    
    $$.getJSON(url, function (data) {
        data = JSON.parse(data);
        $$.each(data.indicadores, function(i,indicador){
            $$('<div />')
            .attr('class', 'row no-gutter body-table')
            .append(
                $$('<div />')
                .attr('class', 'col-25 text-center')
                .html(indicador.indicador)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-25 text-center')
                .html(indicador.anterior)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-25 text-center')
                .html(indicador.como_vamos)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-25 text-center')
                .html(
                    '<img src="img/tendencia_'+indicador.tendencia+'.svg" alt="">'
                )
            )
            .appendTo('#tabla_indicadores');
            
        });
        
    });
    
});

var meses = [
    "",
    "Enero",
    "Marzo",
    "Abril",
    "Mayo",
    "Junio",
    "Julio",
    "Agosto",
    "Septiembre",
    "Octubre",
    "Noviembre",
    "Diciembre",
];

myApp.onPageBeforeRemove('canasto-iscam', function(e){
    $$('#toolbar-footer').hide();
});

var periodoGLB = '';

myApp.onPageInit('canasto-iscam', function (e) {
    
    arreglo_canastos = [];
    
    $$('#toolbar-footer').show();
    
    // var d = new Date();
    // var mes = meses[d.getMonth()];
    // var anio = d.getFullYear();
    // var fecha = mes + " " + 
    
    var url = root_url + 'canastos';
    $$('#lista_canastos .body-table').remove();
    $$.getJSON(url, function (data) {
        data = JSON.parse(data);
        periodoGLB = data.periodo;
        $$('#periodo_general_canasto').html(data.periodo);
        $$.each(data.canastos, function(i,canasto){
            var promedio = canasto.promedios[0];
            
            var obj_promedio = {
                bimestral : canasto.bimestral,
                periodo : canasto.periodo,
                con_volumen : canasto.con_volumen,
                nombre : promedio.post_title
            };
            
            if(canasto.bimestral == 'si'){
                var bim = promedio.bim;
                var acotacion = '*';
            }else{
                var bim = promedio.mes;
                var acotacion = '';
            }
            
            
            arreglo_canastos[canasto.cat_id] = obj_promedio;
            
            var clase_bim = '';
            if(  bim  < 0 ){
                clase_bim = 'font-red';
            }
            
            var clase_ytd = '';
            if( promedio.ytd < 0 ){
                clase_ytd = 'font-red';
            }
            
            var clase_ry = '';
            if( promedio.ry < 0 ){
                clase_ry = 'font-red';
            }
            
            $$('<a />')
            .attr('href', 'detalle-canasto.html')
            .attr('onclick', 'set_category( "'+canasto.cat_id+'", "'+canasto.cat+'" )')
            .attr('class','row no-gutter body-table')
            .append(
                $$('<div />')
                .attr('class', 'col-30 text-left')
                .html(canasto.post_title + ' ' + acotacion )
            )
            .append(
                $$('<div />')
                .attr('class', 'col-20 text-center ' + clase_bim)
                .html(bim)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-20 text-center ' + clase_ytd)
                .html(promedio.ytd)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-20 text-center ' + clase_ry)
                .html(promedio.ry)
            )
            .append(
                $$('<div />')
                .attr('class', 'col-10 text-center')
                .html('<img src="img/blue-arrow.svg" alt="">')
            )
            .appendTo('#lista_canastos');
        });
        
    });
    
});

myApp.onPageInit('detalle-canasto', function (e) {
    var url = root_url + 'canasto/' + cat_id + '/' + cat;
    $$('#area_detalle_canasto .detalle_canasto').remove();
    
    var datos_canasto = arreglo_canastos[cat_id];
    
    $$('#periodo_canasto').html(datos_canasto.periodo);
    // $$('#periodo_canasto').html(periodoGLB);
    
    if(datos_canasto.bimestral == 'si'){
        $$('#titulo_detalle_canasto').html('Reporte Bimestral');
    }else{
        $$('#titulo_detalle_canasto').html('Reporte Mensual');
    }
    
    $$.getJSON(url, function (data) {
        data = JSON.parse(data);    
        
        var canastos = data.canastos;
        
        if(datos_canasto.con_volumen == 'si'){
            promedios_por_valor(datos_canasto, canastos);
            promedios_por_volumen(datos_canasto, canastos);
        }else{
            promedios_por_valor(datos_canasto, canastos);
        }
        
    });
    
});

function set_category(var_cat_id, var_cat){
    cat_id = var_cat_id;
    cat = var_cat;
}

function set_macro_id(id){
    macro_id = id;
}

function loading(){
    myApp.showPreloader('Por favor espere...');
}

function hide_loading(){
    myApp.hidePreloader();
}

function contacto(){
    
    var url = 'http://consejeroscomerciales.com/mail-contacto/';
    
    var formData = myApp.formToJSON('#form_contacto');
    var flag_ok = true;
    
    if(formData.nombre == ''){
        myApp.alert('Por favor captura un nombre.', 'Error!');
        flag_ok = false;
    }
    
    if(flag_ok){
        if(formData.correo == ''){
            myApp.alert('Por favor captura un correo.', 'Error!');
            flag_ok = false;
        }
    }
    
    if(flag_ok){
        if(formData.mensaje == ''){
            myApp.alert('Por favor captura un mensaje.', 'Error!');
            flag_ok = false;
        }
    }
    
    if(flag_ok){
        loading();
        $$.post(url, {nombre:formData.nombre, correo: formData.correo, mensaje:formData.mensaje}, function (data) {
            hide_loading();
            mainView.router.load({
                url: 'index.html'
            });
            myApp.alert('Su mensaje ha sido enviado correctamente.', 'Aviso');
        });
    }
    
    // $$('#form_contacto').submit();    
}

function promedios_por_valor(datos_canasto, canastos){
    
    if(datos_canasto.con_volumen == 'si'){
        
        $$('<div />')
            .attr('class', 'detalle_canasto page-title-iscam-clear')
            .append(
                $$('<h1 />')
                .append(
                    $$('<h2 />')
                    .html(datos_canasto.nombre + '<br />Valor')
                )
            )
        .appendTo('#area_detalle_canasto');
        
        
    }else{
        $$('<div />')
            .attr('class', 'detalle_canasto page-title-iscam-clear')
            .append(
                $$('<h1 />')
                .append(
                    $$('<h2 />')
                    .html(datos_canasto.nombre + ' <br /> Valor')
                )
            )
        .appendTo('#area_detalle_canasto');
    }
    
    
    
    $$.each(canastos, function(i,canasto){
        $$('<div />')
            .attr('class', 'detalle_canasto bloque-info text-right font-blue-light')
            .html(canasto.name)
        .appendTo('#area_detalle_canasto');
        
        var div_titulos = $$('<div />')
            .attr('class', 'detalle_canasto row no-gutter header-table');
            
            var titulos = '<div class="col-30 text-center">CATEGORÍA</div>'+
                '<div class="col-20 text-center">%MIX</div>';
                if(datos_canasto.bimestral == 'si'){
                    titulos += '<div class="col-20 text-center">BIM </div>';
                }else{
                    titulos +=  '<div class="col-20 text-center">MES </div>';
                }
                
                titulos += '<div class="col-20 text-center">YTD</div>'+
                '<div class="col-10 text-center">RY</div>';
        
        $$(div_titulos)
        .html(titulos)
        .appendTo('#area_detalle_canasto');
        
        var promedios = canasto.promedios;
        
        $$.each(promedios, function(indice, promedio){
            
            var div_promedio = 
            $$('<div />')
            .attr('class', 'detalle_canasto row no-gutter header-table');
            
            var clase_mix = '';
            if( promedio.mix < 0 ){
                clase_mix = 'font-red';
            }
            
            var nombre_promedio = promedio.nombre;
            
            var indexof = nombre_promedio.indexOf(" ");
            if(indexof == -1){
                nombre_promedio = dividir_nombre( nombre_promedio );
            }
            
            var datos_promedio = 
            '<div class="col-30 text-center">'+nombre_promedio+'</div>'+
            '<div class="col-20 text-center '+clase_mix+'">'+promedio.mix+'</div>';
            if(datos_canasto.bimestral == 'si'){
                var clase_mes = '';
                if( promedio.bim < 0 ){
                    clase_mes = 'font-red';
                }
                datos_promedio += '<div class="col-20 text-center '+clase_mes+'">'+promedio.bim+'</div>';
            }else{
                var clase_mes = '';
                if( promedio.mes  < 0 ){
                    clase_mes = 'font-red';
                }
                datos_promedio += '<div class="col-20 text-center '+clase_mes+'">'+promedio.mes+'</div>';
            }
            
            var clase_ytd = '';
            if( promedio.ytd < 0 ){
                clase_ytd = 'font-red';
            }
            
            var clase_ry = '';
            if(  promedio.ry < 0 ){
                clase_ry = 'font-red';
            }
            
            datos_promedio += '<div class="col-20 text-center '+clase_ytd+'">'+promedio.ytd+'</div>'+
            '<div class="col-10 text-center '+clase_ry+'">'+promedio.ry+'</div>';
            
            $$(div_promedio)
            .html(datos_promedio)
            .appendTo('#area_detalle_canasto');
            
            // console.log(promedio);
        });
    });
    
}

function promedios_por_volumen(datos_canasto, canastos){
    
    $$('<div />')
        .attr('class', 'detalle_canasto page-title-iscam-clear')
        .append(
            $$('<h1 />')
            .append(
                $$('<h2 />')
                .html(datos_canasto.nombre + ' <br /> Volúmen')
            )
        )
    .appendTo('#area_detalle_canasto');
    
    $$.each(canastos, function(i,canasto){
        $$('<div />')
            .attr('class', 'detalle_canasto bloque-info text-right font-blue-light')
            .html(canasto.name)
        .appendTo('#area_detalle_canasto');
        
        var div_titulos = $$('<div />')
            .attr('class', 'detalle_canasto row no-gutter header-table');
            
            var titulos = '<div class="col-30 text-center">CATEGORÍA</div>'+
                '<div class="col-20 text-center">%MIX</div>';
                if(datos_canasto.bimestral == 'si'){
                    titulos += '<div class="col-20 text-center">BIM </div>';
                }else{
                    titulos +=  '<div class="col-20 text-center">MES </div>';
                }
                
                titulos += '<div class="col-20 text-center">YTD</div>'+
                '<div class="col-10 text-center">RY</div>';
        
        $$(div_titulos)
        .html(titulos)
        .appendTo('#area_detalle_canasto');
        
        var promedios = canasto.promedios;
        
        $$.each(promedios, function(indice, promedio){
            
            var div_promedio = 
            $$('<div />')
            .attr('class', 'detalle_canasto row no-gutter header-table');
            
            var clase_mix = '';
            if( promedio.mix < 0 ){
                clase_mix = 'font-red';
            }
            
            var clase_mes = '';
            if( promedio.mes_volumen  < 0 ){
                clase_mes = 'font-red';
            }
            
            var clase_ytd = '';
            if( promedio.ytd_volumen < 0 ){
                clase_ytd = 'font-red';
            }
            
            var clase_ry = '';
            if(  promedio.ry_volumen < 0 ){
                clase_ry = 'font-red';
            }
            
            var datos_promedio = 
            '<div class="col-30 text-center">'+promedio.nombre+'</div>'+
            '<div class="col-20 text-center '+clase_mix+'">'+promedio.mix+'</div>';
            datos_promedio += '<div class="col-20 text-center '+clase_mes+'">'+promedio.mes_volumen+'</div>';
            datos_promedio += '<div class="col-20 text-center '+clase_ytd+'">'+promedio.ytd_volumen+'</div>'+
            '<div class="col-10 text-center '+clase_ry+'">'+promedio.ry_volumen+'</div>';
            
            $$(div_promedio)
            .html(datos_promedio)
            .appendTo('#area_detalle_canasto');
            
            console.log(promedio);
        });
    });
}

function dividir_nombre(nombre){
    
    var cont = 0;
    var tamanio = nombre.length;
    var nuevo_nombre = '';
    
    for(var i = 0; i < tamanio; i++){
        nuevo_nombre += nombre[i];
        cont++;
        if(cont == 9){
            nuevo_nombre += "-";
            cont = 0;
        }
    }
    
    var nuevo_tamanio = nuevo_nombre.length;
    
    
    if(nuevo_nombre[nuevo_tamanio - 1] == '-'){
        nuevo_nombre = nuevo_nombre.slice(0, -1);
    }
    
    nuevo_nombre = nuevo_nombre.replace(/-/gi, "-<br />");
    
    return nuevo_nombre;
}

$$('form_contacto').on('submitted', function (e) {
  var xhr = e.detail.xhr; // actual XHR object
 
  var data = e.detail.data; // Ajax response from action file
  alert(data);
  // do something with response data
});

