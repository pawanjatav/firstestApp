function actualizarImagen(imagen) {
    var url = ruta_app + 'usuarios/update_image';
    var id_usuario = get_item('id_usuario');
    myApp.showPreloader('Cargando...');
    $$.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: { id_usuario: id_usuario, imagen: imagen },
        success: function (response) {
            console.log(response);
            if (response.codigo == 0) {
                set_item('imagen', imagen);
          
                pintarImagenPerfil();
            }
            if (response.codigo == 1) {
                myApp.alert('Imagen no actualizada correctamente', 'Informaci&#243;n');
            }
            myApp.hidePreloader();
        },
        error: function () {
            myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.', ' ');
        }
    });
}

function onPhotoDataSuccess(imageURI) {
    actualizarImagen(imageURI);
}

function onFail(message) {
    myApp.alert('Error al tomar la foto: ' + message, " ");
}

function tomarImagen() {
    $$("div#CameraOptions").show();
}
function setProfilePicture() {
    $$("div#CameraOptions").hide();
 
        navigator.camera.getPicture(profilePictureSuccess, profilePictureFail, {
            quality: 50,
            correctOrientation: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 365,
            targeHeight: 365,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.PHOTOLIBRARY,
            mediaType: Camera.MediaType.PICTURE
        });
    
}

 
function takeFromCamera() {
    $$("#CameraOptions").hide();
   
        navigator.camera.getPicture(profilePictureSuccess, profilePictureFail, {
            quality: 50,
            correctOrientation: true,
            encodingType: Camera.EncodingType.JPEG,
            targetWidth: 360,
            targeHeight: 365,
            destinationType: navigator.camera.DestinationType.DATA_URL,
            sourceType: navigator.camera.PictureSourceType.CAMERA,
            mediaType: Camera.MediaType.PICTURE
        });
   
}

function profilePictureFail(ex) {
  
}
 
function profilePictureSuccess(imageUrl) {
    actualizarImagen(imageUrl);
}
myApp.onPageInit('perfil', function (page) {
    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-gris"><div class="font-gris icon-top-menu-size icon-custom-icons-23"></div><span class="font-regular"></span></a></div><div class="center sliding text-claro">Perfil</div><div class="right"></div></div>');
 
 
    $$('img#perfilimags').attr('src', window.localStorage.getItem("imagen"));
    setTimeout(function () {
        $$('img#perfilimags').attr('src', window.localStorage.getItem("imagen"));
       // pintarImagenPerfil();
        getPuntosRecord();
    }, 200);
});


myApp.onPageInit('configuracion', function (page) {
   // alert(window.localStorage.getItem("imagen"))
    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-gris-fuerte"><span class="font-gris-fuerte icon-top-menu-size icon-custom-icons-23"></span></a></div><div class="center sliding">Configuraci&#243;n</div><div class="right"></div></div>');
  
    setTimeout(function() {
        $$('img#configimgs').attr('src', window.localStorage.getItem("imagen"));
        var notification_status = window.localStorage.getItem("notification_status");
        //alert(notification_status + " condition1 " + (notification_status != '') + "condition 2" +(notification_status == '1') );
        if (notification_status != '') {
            if (notification_status == '1') {
                $('input#notification').attr("checked", "checked");
            }
        }
      //  pintarImagenPerfil();
    }, 200);
});
 
 


function InviteAFriend() {
    $$('.sharedDiv').show();
}

function getPuntosRecord() {
    myApp.showPreloader('Cargando...');
    $$.post('http://www.promozite.com/checkfans/usuarios/get', { id_usuario: window.localStorage.getItem("id_usuario") }, function (data) {
        var dt = JSON.parse(data.substring(data.indexOf('{')));
        $$('label#perfilusername').html(window.localStorage.getItem("usuarioName"));
        $$('label#puntosdata').html(dt.result.puntos);
        $$('label#Siguiendodata').html(dt.result.following);
        $$('label#Stars').html(dt.result.star);
        myApp.hidePreloader();
    });
}

function ImagenPerfil() {

    var url = '';
    checkImage("http://promozite.com/checkfans/contenido/user_img?id=" + window.localStorage.getItem("id_usuario"), function () {
        var url = window.localStorage.getItem("imagen");
        if (url != null) {
            url = 'http://promozite.com/checkfans/contenido/user_img?id=' + window.localStorage.getItem("id_usuario");
        }
        else {
            url = 'images/user-bg.svg';
        }
        window.localStorage.setItem("imagen", url);
        $$('.img_perfil').attr('src', url);
        $$('.page-on-center .img_perfil').attr('src', url);
        $$('.page-on-center .img_perfil').attr('src', url);
    }, function () {
     
        if (window.localStorage.getItem("login_FB") != null && window.localStorage.getItem("login_FB")) {
            url = 'https://graph.facebook.com/' + window.localStorage.getItem("id_usuario_fB") + '/picture?type=large';
        }
        else {
            var url = window.localStorage.getItem("imagen");
            if (url != null) {
                url = 'http://promozite.com/checkfans/contenido/user_img?id=' + window.localStorage.getItem("id_usuario");
            }
            else {
                url = 'images/user-bg.svg';
            }
        }

        window.localStorage.setItem("imagen", url);

        $$('.img_perfil').attr('src', url);
        $$('.page-on-center .img_perfil').attr('src', url);
        $$('.page-on-center .img_perfil').attr('src', url);
        //alert(url);
    });
}

function pintarImagenPerfil() {
    
    var imgd = '';
    checkImage("http://promozite.com/checkfans/contenido/user_img?id=" + window.localStorage.getItem("id_usuario"), function () {
        imgd =this.src;//'https://graph.facebook.com/1226490617393864/picture?type=large' ;
        $$('.img_perfil').attr('src',imgd );
        $$('.page-on-center .img_perfil').attr('src', imgd);
        $$('.page-on-center .img_perfil').attr('src', imgd);
        window.localStorage.setItem("imagen", imgd);
        $$('img#configimgs').attr('src', window.localStorage.getItem("imagen"));
        $$('img#perfilimags').attr('src', window.localStorage.getItem("imagen"));
    }, function () {
     //   alert(window.localStorage.getItem("login_FB") + " " + window.localStorage.getItem("id_usuario_fB"));
        if (window.localStorage.getItem("login_FB") != null && window.localStorage.getItem("login_FB")) {

            imgd = 'https://graph.facebook.com/' + window.localStorage.getItem("id_usuario_fB") + '/picture?type=large';
          //  alert(url);
        }
        else {
            imgd = 'images/user-bg.svg';          
        }
        $$('.img_perfil').attr('src', imgd);
            $$('.page-on-center .img_perfil').attr('src', imgd);
            $$('.page-on-center .img_perfil').attr('src', imgd);
            window.localStorage.setItem("imagen", imgd);
            $$('img#configimgs').attr('src', window.localStorage.getItem("imagen"));
            $$('img#perfilimags').attr('src', window.localStorage.getItem("imagen"));
    });





}

function checkImage(src, good, bad) {
    //  alert('call img');
    var img = new Image();
    img.onload = good;
    img.onerror = bad;
    img.src = src;
}
//function pintarImagenPerfil(){
//    var imagen = get_item('imagen');

//    if(imagen != ''){
//        $$('.img_perfil').attr('src', "data:image/jpeg;base64," + imagen);
//        $$('.page-on-center .img_perfil').attr('src', "data:image/jpeg;base64," + imagen);
//        $$('.page-on-center .img_perfil').attr('src', "data:image/jpeg;base64," + imagen);
//    }
//}
