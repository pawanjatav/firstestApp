function cargar_pagina_comentarios(id_contenido) {
    id_contenido_comentarios = id_contenido;
    cargar_pagina('comentarios');
}

function favourite_User(c_id, index, value) {
    myApp.showPreloader('Cargando...');
    var cid = c_id
    var index = index;
    var url = ruta_app + 'usuarios/commentUser';
    var uid = window.localStorage.getItem("id_usuario");
    var point = '1';

    console.log(value);
    setTimeout(function () {

        $$.ajax({
            url: url,
            type: 'POST',
            dataType: "json",
            data: { 'uid': uid, 'point': point, 'cid': cid },
            success: function (response) {
                myApp.hidePreloader();
                var codigo = response.codigo;
                if (codigo == 0) {
                    if (value == 'direct') {
                        $('#cl' + index).css("color", "#FF8C00");
                        $('#cl' + index).attr('id', 'cl');
                        $('#cl' + index).removeClass("comment_like");
                    }
                    else {
                        $('#cll' + index).css("color", "#FF8C00 ");
                        $('#cll' + index).attr('id', 'cll');
                        $('#cll' + index).removeClass("comment_like");
                    }
                    window.localStorage.setItem("comment_favourite", response.codigo_post_id);
                }
            },
            error: function () {
                myApp.hidePreloader();
            }
        });
    }, 3000);
}

function goback() {

}
function mostrar_comentarios() {
    $$('#div_comentarios').html('');
    myApp.showPreloader('Cargando...');
    var url = ruta_app + 'comentarios/get';

    $$.post(url, { id_contenido: id_contenido_comentarios }, function (data) {
        data = JSON.parse(data);
        var comentarios = data.comentarios;
        $$('#div_comentarios').html('');
        var i = 0;
        if (comentarios.length > 0) {
            $$.each(comentarios, function (index, comentario) {

                var div_comentario = $$('<div />')
                .html($$('#ejemplo_comentario').html())
                .attr('class', 'row-comentario');
                var cid = window.localStorage.getItem("comment_favourite");
                // console.log(comentario);
                $$(div_comentario).find('.name-user').html(comentario.usuario);

                $$(div_comentario).find('.content').html(comentario.texto_comentario);

                $$(div_comentario).find('.dato').html(comentario.puntos);
                // console.log("First :: "+ JSON.stringify(comentario));
                //  $$(div_comentario).find('.img-user img').attr('src', "data:image/jpeg;base64," + comentario.imagen);
                $$(div_comentario).find('.img-user img').attr('src', 'http://promozite.com/checkfans/contenido/user_img?id=' + comentario.id_usuario);


                $$(div_comentario).find('.abrir_comentario').click(function () {
                    modal_comentario(
                        comentario.contenido_id_contenido,
                        comentario.artistas_id_artista,
                        comentario.id_comentario
                        );
                });


                var comment_direct_array;
                if (cid != '' && cid != null) {
                    comment_direct_array = cid.split(',');
                }

                if ($.inArray(comentario.id_comentario, comment_direct_array) > -1) {
                    $$(div_comentario).find('.comment_like').attr('id', 'cl' + index).css("color", "#FF8C00 ");
                    //$$(div_comentario).removeClass("comment_like");
                    $$(div_comentario).find('.comment_like').attr('id', 'cl');

                }
                else {
                    $$(div_comentario).find('.comment_like').attr('id', 'cl' + index);
                }
                $$('body').on('click', '#cl' + index, function () {
                    favourite_User(
                        comentario.id_comentario, index, 'direct'
                        );
                });
                // id_comentario
                i++;
                $$(div_comentario)
                .appendTo('#div_comentarios');

                var respuestas = comentario.respuestas;
                $$.each(respuestas, function (index, respuesta) {
                    var div_respuesta = $$('<div />')
                    .html($$('#ejemplo_respuesta').html())
                    .attr('class', 'row-comentario force-respuesta-comentario comment_like');
                    $$(div_respuesta).find('.comment_like').attr('id', 'cll' + index);
                    var comment_array;
                    if (cid != '' && cid != null) {
                        comment_array = cid.split(',');
                    }
                    if ($.inArray(comentario.respuestas, comment_array) > -1) {
                        $('#cll' + index).css("color", "#FF8C00 ");
                        $('#cll' + index).removeClass("comment_like");
                        $$(div_respuesta).find('.comment_like').attr('id', 'cll' + index).css("color", "#FF8C00 ");
                        $$(div_respuesta).find('.comment_like').attr('id', 'cll');
                    }
                    else {
                        $$(div_respuesta).find('.comment_like').attr('id', 'cll' + index);
                    }




                    $$('body').on('click', '#cll' + index, function () {
                        favourite_User(
                            respuesta.id_comentario, index, 'reply'
                            );
                    });
                    $$(div_respuesta).find('.name-user').html(respuesta.usuario);
                    $$(div_respuesta).find('.content').html(respuesta.texto_comentario);
                    $$(div_respuesta).find('.dato').html(respuesta.puntos);
                    // console.log("Second :: " + JSON.stringify(respuesta));
                    //   $$(div_respuesta).find('.img-user img').attr('src', "data:image/jpeg;base64," + respuesta.imagen);
                    $$(div_respuesta).find('.img-user img').attr('src', 'http://promozite.com/checkfans/contenido/user_img?id=' + respuesta.id_usuario);

                    $$(div_respuesta)
                    .appendTo('#div_comentarios')

                });

            });

            myApp.hidePreloader();
        }
        else {
            myApp.alert('No hay Comentarios disponibles', 'Comentarios');
        }
        myApp.hidePreloader();
    });
    $('.navbar').html(' <div class="navbar-inner"><div class="left"><a href="#" class="link back"><div class="font-gris-fuerte icon-top-menu-size icon-custom-icons-10"></div></a></div><div class="center sliding">Comentarios</div><div class="right"></div></div>');
   
}

function modal_comentario(id_contenido, id_artista, id_comentario) {
    id_contenido_glb = id_contenido;
    id_artista_glb = id_artista;
    id_comentario_glb = id_comentario;

    $$('#textarea_comentario').val('');

    myApp.popup('.popup-nuevo-mensaje');
}

function enviar_comentario() {
    var id_usuario = get_item('id_usuario');
    var texto_comentario = $$('#textarea_comentario').val();

    var url = ruta_app + 'comentarios/put';

    var data = {
        id_usuario: id_usuario,
        texto_comentario: texto_comentario,
        id_contenido: id_contenido_glb,
        id_artista: id_artista_glb,
        id_comentario: id_comentario_glb

    };
    cargar_pagina_comentarios(id_contenido_glb);
    myApp.showPreloader('Cargando...');
    $$.ajax({
        url: url,
        type: 'POST',
        dataType: 'json',
        data: data,
        success: function (response) {
            if (response.codigo == 0) {
                var comentario = response.comentario;

                if (comentario.comentarios_id_comentario != 0) {
                    mostrar_comentarios();
                }

                myApp.hidePreloader();
                myApp.closeModal('.popup-nuevo-mensaje',true);
               


            }
        }
    });
}


myApp.onPageInit('comentarios', function (page) {
    $$('#div_comentarios').html('');
    //slideToClickedSlide: true

    setTimeout(function () {
        mostrar_comentarios();

    }, 500);

});