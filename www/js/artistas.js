var artistas_nuevos = new Array();
var artistas_seguidos = new Array();
var artistas_eliminar = new Array();
function dstrToUTC(ds) {
    var ds_datearr = ds.split(" ");
    var dsarr = ds_datearr[0].split("-");
    var dsarr_time = ds_datearr[1].split(":");
    var mm = parseInt(dsarr[1], 10);
    var dd = parseInt(dsarr[2], 10);
    var yy = parseInt(dsarr[0], 10);
    var minute = parseInt(dsarr_time[1], 10);
    var second = parseInt(dsarr_time[2], 10);
    var hour = parseInt(dsarr_time[0], 10);
    return Date.UTC(yy, mm - 1, dd, hour, minute, second);
}
function datediff(ds1, ds2) {
    var d1 = dstrToUTC(ds1);
    var d2 = dstrToUTC(ds2);
    var oneday = 86400000;
    return (d2 - d1) / oneday;
}

function agregar_artista(id) {
    
    var index = artistas_nuevos.indexOf(id);    
    // Si est&#225; se elimina
    if( index != -1 ){
        $$('#box-artista-'+id).find('.footer').removeClass('item-active');
        var indice_seguido = artistas_seguidos.indexOf(id);
        var indice_eliminar = artistas_eliminar.indexOf(index);        
        if( indice_seguido != -1 ){
            if( indice_eliminar == -1 ){
                artistas_eliminar.push(id);
            }
        }        
        artistas_nuevos.splice(index, 1);
    }else{
        $$('#box-artista-'+id).find('.footer').addClass('item-active');
        if( indice_eliminar != -1 ){
            artistas_eliminar.splice(indice_eliminar, 1);
        }
        artistas_nuevos.push(id);
    }
}

function seguir_artistas () {
    
    if(artistas_nuevos.length <= 0){
        myApp.alert( 'Debes elegir al menos un artista', 'Mensaje' );
    }else{
        var url = ruta_app + 'artistas/seguir';        
        var id_usuario = get_item('id_usuario');        
        var id_artistas = artistas_nuevos.join('~');        
        if(artistas_eliminar.length == 0){
            var id_artistas_eliminar = '-1';
        }else{
            var id_artistas_eliminar = artistas_eliminar.join('~');
        }
        
        $$.ajax({
            url: url,
            type:'POST',
            dataType:'json',
            data:{ id_usuario: id_usuario, artistas: id_artistas, artistas_eliminar: id_artistas_eliminar  },
            success: function (response) {
                //window.localStorage.removeItem("artist_id786");
                window.localStorage.setItem("artist_id786", response.codigo);
                cargar_pagina('Feed');
            },
            error: function  () {
                myApp.alert('Error de comunicaci&#243;n, verifica tu conexi&#243;n.',' ');
            }
        });
        
    }
    
}

myApp.onPageInit('seleccion_artista', function (page) {
    // run createContentPage func after link was clicked
    // artistas_nuevos
    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-gris-fuerte"><span class="font-gris-fuerte icon-top-menu-size icon-custom-icons-23"></span></a></div><div class="center sliding">Seleccionar Artistas</div><div class="right"></div></div>');

    myApp.showPreloader('Cargando...');
    setTimeout(function  () {
        var url = ruta_app + 'artistas/get';
        $$('[data-page="seleccion_artista"] .page-content').html('');        
        artistas_nuevos = [];        
        var id_usuario = get_item('id_usuario');
        artistas_seguidos = [];
        $$.post(url, { id_usuario: id_usuario }, function (data) {
            data = JSON.parse(data);
            var artistas = data.artistas;
            myApp.hidePreloader();
            $$.each(artistas, function (index, artista) {
                if (artista.usuarios_id_usuario == 0) {
                    clase = ' ';
                } else {
                    clase = 'item-active';
                    artistas_nuevos.push(artista.id_artista);
                    //  artistas_seguidos.push(artista.id_artista);
                }
                if (artista.usuarios_id_usuario == 0) {
                    clase = ' ';
                }
                var string = artista.imagen.search("promozite.com/checkfans/");
                if (string == -1) {
                    var imagesname = ruta_app + 'res/images/artistas/' + artista.imagen;
                } else {
                    var imagesname = artista.imagen;
                }
                $$('<div>')
                .addClass('box-artista')
                .attr('id', 'box-artista-' + artista.id_artista)
                .css('background-image', 'url(' + imagesname + ')')
                .append(
                    $$('<div>').attr('class', 'mask')
                )
                .append(
                    $$('<div>').attr('class', 'footer ' + clase)
                    .html(artista.nombre_artista)
                )
                .click(function () {
                    agregar_artista(artista.id_artista);
                })
                .appendTo('[data-page="seleccion_artista"] .page-content');
            });
            $$('[data-page="seleccion_artista"] .page-content').append();
        });
    }, 500);
    
});


myApp.onPageInit('artistas_lista', function (page) {
    $('.navbar').html('<div class="navbar-inner"><div class="left"><a href="#" class="link icon-only open-panel font-gris-fuerte"><span class="font-gris-fuerte icon-top-menu-size icon-custom-icons-23"></span></a></div><div class="center sliding">Artistas</div><div class="right">  <a href="seleccion_artista.html" class="link icon-only"><div class="font-gris icon-top-menu-size icon-custom-icons-21-03"></div></a></div></div>');
    myApp.showPreloader('Cargando...');
    setTimeout(function  () {
        var url = ruta_app + 'ranking/get/10';
        $$('[data-page="artistas_lista"] .page-content').html('');
        artistas_nuevos = [];        
        var id_usuario = get_item('id_usuario');
        artistas_seguidos = [];
        $$.post(url, { id_usuario: id_usuario }, function (data) {
       data = JSON.parse(data);
       
            myApp.hidePreloader();
            if (data.codigo == 0) {
                var artistas = data.artistas;
                var last_feed = data.last_feed;
              //       console.log(data);
                var contentss = '';
                contentss = '<div class="lista_artistas">';
                contentss += '<ul>';
                $.each(artistas, function (index, swipe) {

                    if (index < artistas.length) {
                        var artista = artistas[index];
                        var contentlast = last_feed[artista.id_artista];
                        // get time last feed
                        var d = new Date();
                        var curr_date = d.getDate();
                        var curr_month = d.getMonth();/* Returns the month (from 0-11) */
                        var curr_month_plus = curr_month + 1; /* because if the month is  4 it will show output 3 so we have to add +1 with month*/
                        var curr_year = d.getFullYear();
                        var curr_hour = d.getHours();
                        var curr_min = d.getMinutes();
                        var curr_sec = d.getSeconds();
                        var a = curr_year + '-' + curr_month_plus + '-' + curr_date + ' ' + curr_hour + ':' + curr_min + ':' + curr_sec;
                        var c1 = datediff(contentlast.last_feed_time, a);
                        var date_difference = '';
                        if (c1 < 1) {
                            var d2 = c1 * 24 * 60;
                            date_difference = 'Today';
                        }
                        else if (c1 > 1 && c1 < 2) {
                            var d2 = c1 / 600000;
                            date_difference = '1 day';
                        }
                        else {
                            date_difference = parseInt(c1, 10) + ' days';
                        }
                        var string = artista.imagen.search("promozite.com/checkfans/");
                        if (string == -1) {
                            var imagesname = ruta_app + 'res/images/artistas/' + artista.imagen;
                        } else {
                            var imagesname = artista.imagen;
                        }
                        contentss += '<li>';
                        contentss += '<div class="img-user">';
                        contentss += '<figure onclick="goToFeed('+artista.id_artista+')">';
                        contentss += '<img src="' + imagesname + '" alt="">';
                        contentss += '</figure>';
                        contentss += '</div>';
                        contentss += '<div class="name-user" onclick="goToFeed(' + artista.id_artista + ')">';
                        contentss += '<p>' + artista.nombre_artista + '</p>';
                        contentss += '<small>'+ contentlast.last_feed_origen_name + ' | ' + date_difference +'</small>';
                        contentss += '</div>';
                        contentss += '</li>';
                    }
                });
                contentss += '</ul> </div>';
            }
            $$('[data-page="artistas_lista"] .page-content').html(contentss);
            });
    }, 500);

});

function goToFeed(ArtistID)
{

    window.localStorage.setItem("FollowAritist_ID", ArtistID);
    window.localStorage.removeItem("FeedActiveIndex");
    window.localStorage.removeItem("FeedIndexID");
    cargar_pagina('Feed');
}




 
 
