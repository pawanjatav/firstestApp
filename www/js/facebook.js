/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
 
var app_f = {
    // Application Constructor
    initialize: function() {
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
        document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicitly call 'app.receivedEvent(...);'
    onDeviceReady: function() {
        app.receivedEvent('deviceready');
    },
    getInfo: function(){
        facebookConnectPlugin.api("/me?fields=name, picture", null,
                 function (result) {
        	//alert("Result: " + JSON.stringify(result));
          alert(result.name);
          //alert("picture " + JSON.stringify(result.picture));
          alert(result.picture.data.url);
          
          
              }, function (error) {
                 console.error("Failed: ", error);
                  }
                 );
    },
    
    login: function(){
//    	var fbLoginSuccess = function (userData) {
//            console.log("UserInfo: ", userData);
//           //alert(JSON.stringify(userData));
//            }
//             
//            facebookConnectPlugin.login(["public_profile"], fbLoginSuccess,
//              function loginError (error) {
//                console.error(error)
//              }
//            );
    	facebookConnectPlugin.login( ["public_profile"],
                function (response) { console.log(JSON.stringify(response)) },
                function (response) { console.log(JSON.stringify(response)) });
    	
    },
    
    logout: function(){
    	facebookConnectPlugin.logout( 
                function (response) { console.log(JSON.stringify(response)) },
                function (response) { console.log(JSON.stringify(response)) });
    },
    
    // Update DOM on a Received Event
    receivedEvent: function(id) {
            
          var parentElement = document.getElementById(id);
        var listeningElement = parentElement.querySelector('.listening');
        var receivedElement = parentElement.querySelector('.received');

        listeningElement.setAttribute('style', 'display:none;');
        receivedElement.setAttribute('style', 'display:block;');

        console.log('Received Event: ' + id);
    }
    ,
    sharewithFB: function (cap, des) {

        myApp.showPreloader('Cargando...');
        facebookConnectPlugin.showDialog({
            method: "share",
            href: "http://promozite.com/checkfans",
            caption:cap,
            description:des,
            picture: 'http://promozite.com/checkfans/res/images/main_logo.png'

        }, function (response) {
            myApp.hidePreloader();
            $$('.sharedDiv').hide();
        },
                function (response) {  myApp.hidePreloader(); $$('.sharedDiv').hide(); });;
    }
    ,
    GetLoginStatus: function () {
        var fbLoginstatus = '';
        facebookConnectPlugin.getLoginStatus(function (response) { fbLoginstatus = response.status; alert(JSON.stringify(response)) },
                function (response) { alert(JSON.stringify(response)) });

        return fbLoginstatus;


        console.log('Received Event: ' + id);
    }
};

app.initialize();
