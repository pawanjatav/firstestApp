# ************************************************************
# Sequel Pro SQL dump
# Versión 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.7.13)
# Base de datos: checkfans
# Tiempo de Generación: 2016-10-17 11:14:31 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Volcado de tabla admin
# ------------------------------------------------------------

DROP TABLE IF EXISTS `admin`;

CREATE TABLE `admin` (
  `id_admin` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`id_admin`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `admin` WRITE;
/*!40000 ALTER TABLE `admin` DISABLE KEYS */;

INSERT INTO `admin` (`id_admin`, `usuario`, `password`, `correo`)
VALUES
	(1,'usuario','f8d0c886096c59ec6d42229c870b571f','admin@checkfans.com');

/*!40000 ALTER TABLE `admin` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla artistas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artistas`;

CREATE TABLE `artistas` (
  `id_artista` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_artista` varchar(75) DEFAULT NULL,
  `imagen` varchar(100) DEFAULT NULL,
  `facebook` varchar(250) DEFAULT NULL,
  `twitter` varchar(250) DEFAULT NULL,
  `instagram` varchar(250) DEFAULT NULL,
  `youtube` varchar(250) DEFAULT NULL,
  `spotify` varchar(250) DEFAULT NULL,
  `fecha_alta` datetime DEFAULT NULL,
  PRIMARY KEY (`id_artista`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `artistas` WRITE;
/*!40000 ALTER TABLE `artistas` DISABLE KEYS */;

INSERT INTO `artistas` (`id_artista`, `nombre_artista`, `imagen`, `facebook`, `twitter`, `instagram`, `youtube`, `spotify`, `fecha_alta`)
VALUES
	(1,'LALI ESPOSITO','image-artist1.png','https://www.facebook.com/oscar.roquentin','','','','','2016-09-25 13:28:55'),
	(2,'MARAMA','image-artist2.png','https://www.facebook.com/oscar.roquentin','','','','','2016-09-25 13:31:05');

/*!40000 ALTER TABLE `artistas` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla artistas_has_generos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artistas_has_generos`;

CREATE TABLE `artistas_has_generos` (
  `artistas_id_artista` int(11) NOT NULL,
  `generos_id_genero` int(11) NOT NULL,
  PRIMARY KEY (`artistas_id_artista`,`generos_id_genero`),
  KEY `fk_artistas_has_generos_generos1_idx` (`generos_id_genero`),
  KEY `fk_artistas_has_generos_artistas_idx` (`artistas_id_artista`),
  CONSTRAINT `fk_artistas_has_generos_artistas` FOREIGN KEY (`artistas_id_artista`) REFERENCES `artistas` (`id_artista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_artistas_has_generos_generos1` FOREIGN KEY (`generos_id_genero`) REFERENCES `generos` (`id_genero`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `artistas_has_generos` WRITE;
/*!40000 ALTER TABLE `artistas_has_generos` DISABLE KEYS */;

INSERT INTO `artistas_has_generos` (`artistas_id_artista`, `generos_id_genero`)
VALUES
	(1,1),
	(2,1);

/*!40000 ALTER TABLE `artistas_has_generos` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla artistas_relacionados
# ------------------------------------------------------------

DROP TABLE IF EXISTS `artistas_relacionados`;

CREATE TABLE `artistas_relacionados` (
  `id_artistas_relacionados` int(11) NOT NULL AUTO_INCREMENT,
  `id_artista_1` int(11) DEFAULT NULL,
  `id_artista_2` int(11) DEFAULT NULL,
  PRIMARY KEY (`id_artistas_relacionados`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;



# Volcado de tabla comentarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `comentarios`;

CREATE TABLE `comentarios` (
  `id_comentario` int(11) NOT NULL AUTO_INCREMENT,
  `usuarios_id_usuario` int(11) NOT NULL,
  `contenido_id_contenido` int(11) NOT NULL,
  `artistas_id_artista` int(11) NOT NULL,
  `comentarios_id_comentario` varchar(45) DEFAULT '0',
  `texto_comentario` varchar(250) DEFAULT NULL,
  `fecha_comentario` varchar(45) DEFAULT NULL,
  `activo` enum('si','no') DEFAULT 'si',
  PRIMARY KEY (`id_comentario`,`usuarios_id_usuario`),
  KEY `fk_commentarios_usuarios1_idx` (`usuarios_id_usuario`),
  KEY `fk_commentarios_contenido1_idx` (`contenido_id_contenido`,`artistas_id_artista`),
  CONSTRAINT `fk_commentarios_contenido1` FOREIGN KEY (`contenido_id_contenido`, `artistas_id_artista`) REFERENCES `contenido` (`id_contenido`, `artistas_id_artista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_commentarios_usuarios1` FOREIGN KEY (`usuarios_id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `comentarios` WRITE;
/*!40000 ALTER TABLE `comentarios` DISABLE KEYS */;

INSERT INTO `comentarios` (`id_comentario`, `usuarios_id_usuario`, `contenido_id_contenido`, `artistas_id_artista`, `comentarios_id_comentario`, `texto_comentario`, `fecha_comentario`, `activo`)
VALUES
	(1,1,1,1,'0','Comentario 1','2016-10-03 23:44:05','si'),
	(25,1,2,1,'0','Comentario Contenido 2','2016-10-03 23:44:05','si'),
	(26,1,3,1,'0','Comentario contenido 3','2016-10-03 23:44:05','si'),
	(29,1,3,1,'26','Holaaaaaa\\n','2016-10-05 09:51:20','si'),
	(30,1,1,1,'1','Hola hola','2016-10-05 09:52:18','si'),
	(31,1,1,1,'0','Guau\\n','2016-10-05 10:07:19','si'),
	(32,1,3,1,'26','Hoel','2016-10-05 14:13:45','si'),
	(33,1,1,1,'0','Hoa','2016-10-06 10:11:08','si');

/*!40000 ALTER TABLE `comentarios` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla contenido
# ------------------------------------------------------------

DROP TABLE IF EXISTS `contenido`;

CREATE TABLE `contenido` (
  `id_contenido` int(11) NOT NULL AUTO_INCREMENT,
  `artistas_id_artista` int(11) NOT NULL,
  `fecha_publicacion` varchar(45) DEFAULT NULL,
  `url_video` varchar(45) DEFAULT '',
  `foto` varchar(45) DEFAULT NULL,
  `contenido` text,
  `titulo` varchar(250) DEFAULT NULL,
  `origen` enum('CheckFans','Facebook','Twitter') DEFAULT NULL,
  `url_origen` varchar(250) DEFAULT '',
  PRIMARY KEY (`id_contenido`,`artistas_id_artista`),
  KEY `fk_contenido_artistas1_idx` (`artistas_id_artista`),
  CONSTRAINT `fk_contenido_artistas1` FOREIGN KEY (`artistas_id_artista`) REFERENCES `artistas` (`id_artista`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `contenido` WRITE;
/*!40000 ALTER TABLE `contenido` DISABLE KEYS */;

INSERT INTO `contenido` (`id_contenido`, `artistas_id_artista`, `fecha_publicacion`, `url_video`, `foto`, `contenido`, `titulo`, `origen`, `url_origen`)
VALUES
	(1,1,'2016-09-30','','foto1.jpg','Contenido capturadooo','Contenido 1 #Artista1','CheckFans',''),
	(2,1,'2016-09-30','','foto1.jpg','Contenido capturadooo','Contenido 1 #Artista1','CheckFans',''),
	(3,1,'2016-09-30','','foto1.jpg','Contenido capturadooo','Contenido 1 #Artista1','CheckFans','');

/*!40000 ALTER TABLE `contenido` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla generos
# ------------------------------------------------------------

DROP TABLE IF EXISTS `generos`;

CREATE TABLE `generos` (
  `id_genero` int(11) NOT NULL AUTO_INCREMENT,
  `nombre_genero` varchar(65) DEFAULT NULL,
  PRIMARY KEY (`id_genero`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `generos` WRITE;
/*!40000 ALTER TABLE `generos` DISABLE KEYS */;

INSERT INTO `generos` (`id_genero`, `nombre_genero`)
VALUES
	(1,'Rock'),
	(2,'Punk'),
	(3,'Pnk 2'),
	(4,'Banda'),
	(5,'Jazz'),
	(6,'Blues'),
	(7,'Clásica');

/*!40000 ALTER TABLE `generos` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla usuarios
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios`;

CREATE TABLE `usuarios` (
  `id_usuario` int(11) NOT NULL AUTO_INCREMENT,
  `usuario` varchar(45) DEFAULT NULL,
  `correo` varchar(45) DEFAULT NULL,
  `password` varchar(45) DEFAULT NULL,
  `fb_id` varchar(45) DEFAULT NULL,
  `instagram_id` varchar(45) DEFAULT NULL,
  `twitter_id` varchar(45) DEFAULT NULL,
  `imagen` varchar(250) DEFAULT NULL,
  PRIMARY KEY (`id_usuario`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `usuarios` WRITE;
/*!40000 ALTER TABLE `usuarios` DISABLE KEYS */;

INSERT INTO `usuarios` (`id_usuario`, `usuario`, `correo`, `password`, `fb_id`, `instagram_id`, `twitter_id`, `imagen`)
VALUES
	(1,'usuario1','correo@mail.com','nuevo123',NULL,NULL,NULL,'url_imagen'),
	(2,'usuario2','correo@mail.com','nuevo123',NULL,NULL,NULL,'url_imagen');

/*!40000 ALTER TABLE `usuarios` ENABLE KEYS */;
UNLOCK TABLES;


# Volcado de tabla usuarios_has_artistas
# ------------------------------------------------------------

DROP TABLE IF EXISTS `usuarios_has_artistas`;

CREATE TABLE `usuarios_has_artistas` (
  `usuarios_id_usuario` int(11) NOT NULL,
  `artistas_id_artista` int(11) NOT NULL,
  PRIMARY KEY (`usuarios_id_usuario`,`artistas_id_artista`),
  KEY `fk_usuarios_has_artistas_artistas1_idx` (`artistas_id_artista`),
  KEY `fk_usuarios_has_artistas_usuarios1_idx` (`usuarios_id_usuario`),
  CONSTRAINT `fk_usuarios_has_artistas_artistas1` FOREIGN KEY (`artistas_id_artista`) REFERENCES `artistas` (`id_artista`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_usuarios_has_artistas_usuarios1` FOREIGN KEY (`usuarios_id_usuario`) REFERENCES `usuarios` (`id_usuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `usuarios_has_artistas` WRITE;
/*!40000 ALTER TABLE `usuarios_has_artistas` DISABLE KEYS */;

INSERT INTO `usuarios_has_artistas` (`usuarios_id_usuario`, `artistas_id_artista`)
VALUES
	(1,2);

/*!40000 ALTER TABLE `usuarios_has_artistas` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
